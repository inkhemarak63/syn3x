<?php

namespace AppBundle\Controller\Web\Frontend;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class KhodeSiteTopController extends KhodeSuperFrontendController
{
    /**
     * @Route(
     *     "/", 
     * )
    */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }
}
