<?php
namespace AppBundle\Util;

use AppBundle\MaseConfig;

class MaseCustoMediaService
{
    private $apiUrl;
    private $apiKey;

    public function __construct()
    {
        $this->apiUrl = MaseConfig::getConstValue('CM_SITE_URL');
        $this->apiKey = MaseConfig::getConstValue('CM_API_KEY');
    }

    public function getRetentionPoint($userId)
    {
        $userInfo = $this->getUserInfoByUserId($userId);
        if (false === $userInfo) {
            return false;
        }
        return $userInfo['point'];
    }

    public function getProfileUrl($userId)
    {
        $userInfo = $this->getUserInfoByUserId($userId);
        if (false === $userInfo) {
            return false;
        }
        return $this->apiUrl . MaseConfig::getConstValue('CM_PROFILE_URL') . $userInfo['local_id'];
    }

    private function getUserInfoByUserId($userId)
    {
        $headers = ['Accept' => 'application/json'];
        $data = [
            'api_pw'         => $this->apiKey,
            'target_user_id' => $userId,
        ];

        $response = null;

        \Unirest\Request::timeout(MaseConfig::getConstValue('WAIT_TIMEOUT_FOR_EXTERNAL_API'));
        try {
            $response = \Unirest\Request::post(
                $this->apiUrl.'/api/userInfo.json',
                $headers,
                $data
            );
        } catch (\Exception $e) {
            return false;
        }

        if (200 !== $response->code) {
            return false;
        }

        $result = [];
        foreach ($response->body as $key => $value) {
            $result[$key] = $value;
        }

        return false === $result['result'] ? false : $result;
    }

    public function getUserProfileByUserId($userId)
    {
        $headers = ['Accept' => 'application/json'];
        $data = [
            'api_pw'         => $this->apiKey,
            'target_user_id' => $userId,
        ];

        $response = null;

        \Unirest\Request::timeout(MaseConfig::getConstValue('WAIT_TIMEOUT_FOR_EXTERNAL_API'));
        try {
            $response = \Unirest\Request::post(
                $this->apiUrl.'/api/profileExt.json',
                $headers,
                $data
            );
        } catch (\Exception $e) {
            return false;
        }

        if (200 !== $response->code) {
            return false;
        }

        $result = [];
        foreach ($response->body as $key => $value) {
            $result[$key] = $value;
        }

        return false === $result['result'] ? false : $result;
    }

    public function useRetentionPoint($userId, $point, $description, $source, $sourceId)
    {
        $headers = ['Accept' => 'application/json'];
        $data = [
            'api_pw'         => $this->apiKey,
            'target_user_id' => $userId,
            'point'          => $point,
            'description'    => $description,
            'is_fouce'       => 0,
            'source'         => $source,
            'source_id'      => $sourceId,
        ];

        $response = null;

        \Unirest\Request::timeout(MaseConfig::getConstValue('WAIT_TIMEOUT_FOR_EXTERNAL_API'));
        try {
            /**
             * 開発メモ
             * 呼び出した先のAPIは、必ず、MaseConfig::getConstValue('WAIT_TIMEOUT_FOR_EXTERNAL_API')より短い時間で
             * 何らかの結果を応答する必要あり。
             */
            $response = \Unirest\Request::post(
                $this->apiUrl.'/api/usePoint.json',
                $headers,
                $data
            );
        } catch (\Exception $e) {
            return false;
        }

        if (200 !== $response->code) {
            return false;
        }

        if (0 !== $response->body->error_code) {
            return false;
        }
        return true;
    }

    public function returnRetentionPoint($userId, $point, $description, $source, $sourceId)
    {
        $headers = ['Accept' => 'application/json'];
        $data = [
            'api_pw'         => $this->apiKey,
            'target_user_id' => $userId,
            'point'          => $point,
            'description'    => $description,
            'pay_type'       => 'system',
            'source'         => $source,
            'source_id'      => $sourceId,
        ];

        $response = null;

        \Unirest\Request::timeout(MaseConfig::getConstValue('WAIT_TIMEOUT_FOR_EXTERNAL_API'));
        try {
            /**
             * 開発メモ
             * 呼び出した先のAPIは、必ず、MaseConfig::getConstValue('WAIT_TIMEOUT_FOR_EXTERNAL_API')より短い時間で
             * 何らかの結果を応答する必要あり。
             */
            $response = \Unirest\Request::post(
                $this->apiUrl.'/api/addPoint.json',
                $headers,
                $data
            );
        } catch (\Exception $e) {
            return false;
        }

        if (200 !== $response->code) {
            return false;
        }

        if (0 !== $response->body->error_code) {
            return false;
        }
        return true;
    }

    public function updateCmPersonInfo($userId, $updateInfo)
    {
        $headers = ['Accept' => 'application/json'];
        $data = [
            'api_pw'         => $this->apiKey,
            'target_user_id' => $userId,
        ];
        foreach ($updateInfo as $key => $val) {
            $data['person['.$key.']'] = $val;
        }

        $response = null;

        \Unirest\Request::timeout(MaseConfig::getConstValue('WAIT_TIMEOUT_FOR_EXTERNAL_API'));
        try {
            /**
             * 開発メモ
             * 呼び出した先のAPIは、必ず、MaseConfig::getConstValue('WAIT_TIMEOUT_FOR_EXTERNAL_API')より短い時間で
             * 何らかの結果を応答する必要あり。
             */
            $response = \Unirest\Request::post(
                $this->apiUrl.'/api/updatePersonInfo.json',
                $headers,
                $data
            );
        } catch (\Exception $e) {
            return false;
        }

        if (200 !== $response->code) {
            return false;
        }

        return $response->body->result;
    }
}
