<?php

namespace AppBundle\Util;

class MaseGlobalInstanceBagService
{
    private $kernel;
    private $container;

    public function __construct($kernel, $container)
    {
        $this->kernel = $kernel;
        $this->container = $container;
    }

    public function getKernel()
    {
        return $this->kernel;
    }

    public function getContainer()
    {
        return $this->container;
    }
}
