<?php
namespace AppBundle\Util;

use AppBundle\MaseConfig;

class MaseFileServerService
{
    private $siteUrl;
    private $apiUrlForDw;
    private $apiKey;

    public function __construct()
    {
        $this->siteUrl = MaseConfig::getConstValue('SITE_URL');
        $this->apiUrlForDw = MaseConfig::getConstValue('DIGITAL_SERVER_URL_DW');
        $this->apiKey = MaseConfig::getConstValue('DIGITAL_SERVER_API_KEY');
    }

    public function download($localId, $ssl = true)
    {
        /**
         * 開発メモ
         * 呼び出した先のAPIは、必ず、MaseConfig::getConstValue('WAIT_TIMEOUT_FOR_EXTERNAL_API')より短い時間で
         * 何らかの結果を応答する必要あり。
         */
        \Unirest\Request::timeout(MaseConfig::getConstValue('WAIT_TIMEOUT_FOR_EXTERNAL_API'));

        $response = null;
        try {
            $response = \Unirest\Request::get(
                ($ssl ? 'https:' : 'http:') . $this->apiUrlForDw,
                [
                    'referer' => $this->siteUrl . '/'
                ],
                [
                    'localId' => $localId,
                    'ak' => $this->apiKey,
                ]
            );
        } catch (\Exception $e) {
            return false;
        }

        if (200 !== $response->code) {
            return false;
        }

        $match = [];
        $fileName = array_key_exists('Content-Disposition', $response->headers) ? (1 === preg_match('/.+filename="{0,1}(.+?)"{0,1};.*/u', $response->headers['Content-Disposition'], $match) ? $match[1] : '') : '';

        return [
            'fileName' => $fileName,
            'body' => $response->raw_body,
            'size' => (int)$response->headers['Content-Length']
        ];
    }
}
