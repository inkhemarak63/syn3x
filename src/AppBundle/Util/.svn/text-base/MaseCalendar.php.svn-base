<?php

namespace AppBundle\Util;

class MaseCalendar
{
    private $options;

    private $year;
    private $month;
    private $day;
    private $contents;
    private $links;
    
    public function __construct($options = [])
    {
        $this->options = $options + [
            'cssCalTable' => 'mase-calendar',
            'cssCalRow' => 'mase-calendar-row',
            'cssCalDayHead' => 'mase-calendar-day-head',
            'cssCalDay' => 'mase-calendar-day',
            'cssCalDayNumber' => 'mase-calendar-day-number',
            'cssCalDayNumberLink' => 'js_mase_dlg_link',
            'cssCalDayBlank' => 'mase-calendar-day-blank',
            'cssCalDayToday' => 'mase-calendar-day-today',
            'cssCalContent' => 'mase-calendar-content',
            'headings' => ['日', '月', '火', '水', '木', '金', '土'],
            'headingNumbers' => [7, 1, 2, 3, 4, 5, 6]
        ];
    }
    
    public function year($year)
    {
        $this->year = $year;
        return $this;
    }
    
    public function month($month)
    {
        $this->month = $month;
        return $this;
    }
    
    public function day($day)
    {
        $this->day = $day;
        return $this;
    }
    
    public function options($options)
    {
        $this->options = $options;
        return $this;
    }
    
    public function setContent($ymd, $content)
    {
        $this->contents[$ymd] = [$content];
        return $this;
    }
    
     public function addContent($ymd, $content)
    {
        $this->contents[$ymd][] = $content;
        return $this;
    }
    
    public function setLink($ymd, $url)
    {
        $this->links[$ymd] = $url;
        return $this;
    }
    
    public static function in($year, $month)
    {
        $calendar = new static;
        $calendar
            ->year($year)
            ->month($month)
        ;
        return $calendar;
    }
    
    public static function today()
    {
        $calendar = new static;
        $calendar
            ->year(date('Y'))
            ->month(date('n'))
            ->day(('j'));
        return $calendar;
    }
    
    public function __toString()
    {
        $year = $this->year;
        $month = $this->month;
        
        extract($this->options);

        $calendar =
            "<table cellpadding='0' cellspacing='0' class='{$cssCalTable}'>" .
            "<tr class='{$cssCalRow}'>" .
            "<td class='{$cssCalDayHead}'>" .
            implode("</td><td class='{$cssCalDayHead}'>", $headings) .
            "</td>" .
            "</tr>";

        $runningDay = date('N', mktime(0, 0, 0, $month, 1, $year));
        $daysInMonth = date('t', mktime(0, 0, 0, $month, 1, $year));

        $calendar .= "<tr class='{$cssCalRow}'>";

        foreach ($headingNumbers as $number) {
            if ($runningDay == $number) {
                break;
            }
            $calendar .= "<td class='{$cssCalDayBlank}'> </td>";
        }

        $pointer = $headingNumbers;
        reset($pointer);
        while (current($pointer) != $runningDay) {
            next($pointer);
        }
        
        for ($day = 1; $day <= $daysInMonth; $day++) {
            $currentDate = date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
            $hasContent = false;
            if (isset($this->contents) && isset($this->contents[$currentDate])) {
                $hasContent = true;
            }
            $hasLink = false;
            if (isset($this->links) && isset($this->links[$currentDate])) {
                $hasLink = true;
            }
            
            if (null !== $this->day && $day == $this->day) {
                $_cssCalDay = $cssCalDay . ' ' . $cssCalDayToday;
            } else {
                $_cssCalDay = $cssCalDay;
            }

            $calendar .= $hasContent ?
                "<td class='{$_cssCalDay} {$cssCalContent}'>" :
                "<td class='{$_cssCalDay}'>";

            $calendar .= $hasLink ?
                "<i class='{$cssCalDayNumber}'><a href='{$this->links[$currentDate]}' class='{$cssCalDayNumberLink}'>{$day}</a></i>" :
                "<i class='{$cssCalDayNumber}'>{$day}</i>";

            if ($hasContent) {
                $text = '';
                foreach ($this->contents[$currentDate] as $content) {
                    $text .= "<span>{$content['text']}</span>";
                }
            
                $calendar .= "<div class='{$cssCalContent}'>{$text}</div>";
            }

            $calendar .= "</td>";

            if ($runningDay == $headingNumbers[6]) {
                $calendar .= "</tr>";
                if (($day + 1) <= $daysInMonth) {
                    $calendar .= "<tr class='{$cssCalRow}'>";
                }
            }
            
            $runningDay = next($pointer) ?: reset($pointer);
        }
        
        do {
            $calendar .= "<td class='{$cssCalDayBlank}'> </td>";
        } while (next($pointer));

        $calendar .= "</tr>";
        
        $calendar .= '</table>';

        return $calendar;
    }
    
}
