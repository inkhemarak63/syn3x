<?php

namespace AppBundle\Repository\Condition\TodoSearch;

use AppBundle\Form\Traits\Conditions\CurrentUser;
use AppBundle\Form\Traits\Conditions\Locale;

class TodoSearchCondition
{
    use Locale;
    use CurrentUser;
}
