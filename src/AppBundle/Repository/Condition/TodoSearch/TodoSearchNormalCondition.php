<?php

namespace AppBundle\Repository\Condition\TodoSearch;

/** 一般ユーザの検索条件 */
final class TodoSearchNormalCondition extends TodoSearchCondition
{
    /** @var string 新着順 */
    const SORT_CREATED_AT = 'created_at';

    /** @var ?string 並び順 */
    private ?string $sort = null;
    /** @var int|null アイテム01(符号有数値)の最小値 */
    private ?int $minItemNumInt01 = null;
    /** @var int|null アイテム01(符号有数値)の最大値 */
    private ?int $maxItemNumInt01 = null;
    /** @var string|null 学部・学科 */
    private ?string $itemCodeVarchar01 = null;
    /** @var string|null 専攻科 */
    private ?string $itemCodeVarchar02 = null;
    /** @var array|null 希望業種 */
    private ?array $itemCodeVarchar06 = null;
    /** @var array|null 希望職種 */
    private ?array $itemCodeVarchar09 = null;
    /** @var string|null 希望勤務地 */
    private ?string $itemCodeVarchar12 = null;

    /**
     * @return string
     */
    public function getSort(): string
    {
        return self::SORT_CREATED_AT;
    }

    /**
     * @param string|null $sort
     * @return TodoSearchNormalCondition
     */
    public function setSort(?string $sort): TodoSearchNormalCondition
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinItemNumInt01(): ?int
    {
        return $this->minItemNumInt01;
    }

    /**
     * @param int|null $minItemNumInt01
     * @return TodoSearchNormalCondition
     */
    public function setMinItemNumInt01(?int $minItemNumInt01): TodoSearchNormalCondition
    {
        $this->minItemNumInt01 = $minItemNumInt01;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxItemNumInt01(): ?int
    {
        return $this->maxItemNumInt01;
    }

    /**
     * @param int|null $maxItemNumInt01
     * @return TodoSearchNormalCondition
     */
    public function setMaxItemNumInt01(?int $maxItemNumInt01): TodoSearchNormalCondition
    {
        $this->maxItemNumInt01 = $maxItemNumInt01;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getItemCodeVarchar01(): ?string
    {
        return $this->itemCodeVarchar01;
    }

    /**
     * @param string|null $itemCodeVarchar01
     * @return TodoSearchNormalCondition
     */
    public function setItemCodeVarchar01(?string $itemCodeVarchar01): TodoSearchNormalCondition
    {
        $this->itemCodeVarchar01 = $itemCodeVarchar01;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getItemCodeVarchar02(): ?string
    {
        return $this->itemCodeVarchar02;
    }

    /**
     * @param string|null $itemCodeVarchar02
     * @return TodoSearchNormalCondition
     */
    public function setItemCodeVarchar02(?string $itemCodeVarchar02): TodoSearchNormalCondition
    {
        $this->itemCodeVarchar02 = $itemCodeVarchar02;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getItemCodeVarchar06(): ?array
    {
        return $this->itemCodeVarchar06;
    }

    /**
     * @param array|null $itemCodeVarchar06
     * @return TodoSearchNormalCondition
     */
    public function setItemCodeVarchar06(?array $itemCodeVarchar06): TodoSearchNormalCondition
    {
        $this->itemCodeVarchar06 = $itemCodeVarchar06;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getItemCodeVarchar09(): ?array
    {
        return $this->itemCodeVarchar09;
    }

    /**
     * @param array|null $itemCodeVarchar09
     * @return TodoSearchNormalCondition
     */
    public function setItemCodeVarchar09(?array $itemCodeVarchar09): TodoSearchNormalCondition
    {
        $this->itemCodeVarchar09 = $itemCodeVarchar09;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getItemCodeVarchar12(): ?string
    {
        return $this->itemCodeVarchar12;
    }

    /**
     * @param string|null $itemCodeVarchar12
     * @return TodoSearchNormalCondition
     */
    public function setItemCodeVarchar12(?string $itemCodeVarchar12): TodoSearchNormalCondition
    {
        $this->itemCodeVarchar12 = $itemCodeVarchar12;
        return $this;
    }
}
