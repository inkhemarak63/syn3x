<?php

namespace AppBundle\Repository\Condition\TodoSearch;

use AppBundle\Form\Traits\Conditions\Keyword;

/** パートナーの検索条件 */
final class TodoSearchPartnerCondition extends TodoSearchCondition
{
    use Keyword;

    /** @var string 新着順 */
    const SORT_CREATED_AT = 'created_at';

    /** @var ?string 並び順 */
    private ?string $sort = null;
    /** @var string|null 創価大学卒業生の有無 */
    private ?string $itemCodeVarchar03 = null;
    /** @var string|null 業種 */
    private ?string $itemCodeVarchar04 = null;

    /**
     * @return string
     */
    public function getSort(): string
    {
        return self::SORT_CREATED_AT;
    }

    /**
     * @param string|null $sort
     * @return TodoSearchPartnerCondition
     */
    public function setSort(?string $sort): TodoSearchPartnerCondition
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getItemCodeVarchar03(): ?string
    {
        return $this->itemCodeVarchar03;
    }

    /**
     * @param string|null $itemCodeVarchar03
     * @return TodoSearchPartnerCondition
     */
    public function setItemCodeVarchar03(?string $itemCodeVarchar03): TodoSearchPartnerCondition
    {
        $this->itemCodeVarchar03 = $itemCodeVarchar03;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getItemCodeVarchar04(): ?string
    {
        return $this->itemCodeVarchar04;
    }

    /**
     * @param string|null $itemCodeVarchar04
     * @return TodoSearchPartnerCondition
     */
    public function setItemCodeVarchar04(?string $itemCodeVarchar04): TodoSearchPartnerCondition
    {
        $this->itemCodeVarchar04 = $itemCodeVarchar04;
        return $this;
    }
}
