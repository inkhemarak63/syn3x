<?php

namespace AppBundle;
use Symfony\Component\Yaml\Yaml;
class KhodeConfig
{
    // public const MASEDEV        = 'MASEDEV_';
    // public const PROD_FILE_PATH = '/../../../prod';
    /**
     * parameters.yml
     */
    private static function laodParametersYml($filePath) {
        return Yaml::parse(file_get_contents($filePath));
    }
    /**
     * getMaseParam
     *
     */
    private static $parameters = null;
    public static function getMaseParam($key) {
        if (null === self::$parameters) {
            self::$parameters = self::laodParametersYml(__DIR__ . '/../../app/config/parameters.yml');
        }

        if (false === array_key_exists($key, self::$parameters['parameters'])) {
            throw new \InvalidArgumentException('does not exist.['.$key.']');
        }
        return self::$parameters['parameters'][$key];
    }
    /**
     * getConstValue
     *
     */
    private static $constValues = null;
    public static function getConstValue($key) {
        if (null === self::$constValues) {
            self::$constValues = self::laodParametersYml(__DIR__ . '/../../app/config/khode_const.yml');
        }
        if (
            false === file_exists(__DIR__ . '/../../../prod') &&
            true  === array_key_exists('MASEDEV_' . $key, self::$constValues['consts'])
        ) {
            return self::$constValues['consts']['MASEDEV_' . $key];
        }
        if (false === array_key_exists($key, self::$constValues['consts'])) {
            throw new \InvalidArgumentException('does not exist.['.$key.']');
        }
        return self::$constValues['consts'][$key];
    }
}
