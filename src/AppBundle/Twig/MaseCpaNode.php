<?php

namespace AppBundle\Twig;

class MaseCpaNode extends \Twig_Node
{
    public function __construct(\Twig_Node_Expression $targetUser, \Twig_Node_Expression $attribute, \Twig_Node $body, $lineno = 0, $tag = null)
    {
        $nodes = [
            'targetUser' => $targetUser, 
            'attribute'  => $attribute,
            'body'       => $body,
        ];
        parent::__construct($nodes, [], $lineno, $tag);
    }

    public function compile(\Twig_Compiler $compiler)
    {
        $compiler
            ->addDebugInfo($this)
            ->write('$aTag = $this->env->getExtension(\'AppBundle\Twig\MaseExtension\')->createAtagForProfileStart('."\n")
            ->subcompile($this->getNode('targetUser'))
            ->raw(",\n")
            ->subcompile($this->getNode('attribute'))
            ->raw("\n")
            ->write(');'."\n")
            ->write('if (\'\' !== $aTag) {'."\n")
            ->write('echo $aTag;'."\n")
            ->subcompile($this->getNode('body'))
            ->write('echo $this->env->getExtension(\'AppBundle\Twig\MaseExtension\')->createAtagEnd();'."\n")
            ->write('}'."\n")
        ;
    }
}
