<?php

namespace AppBundle\Twig;

use AppBundle\Entity\MaseMailMagazineMultiLang;
use AppBundle\Entity\MaseUser;
use AppBundle\Entity\Traits\HasOpenRangeColumns;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;

use AppBundle\Entity\MaseCase;
use AppBundle\Entity\MaseProduct;
use AppBundle\Entity\MaseReserveTargetManage;
use AppBundle\Entity\MaseReserveTarget;
use AppBundle\Security\Voter\Entity\MaseSuperEntityVoter;

use AppBundle\MaseConfig;

use AppBundle\Util\MaseSeesion;
use AppBundle\Util\MaseUtil;
use Symfony\Component\Translation\TranslatorInterface;

class MaseExtension extends \Twig_Extension
{
    public function __construct(
        Router $router,
        RequestStack $requestStack
    ){
        $this->router = $router;
        $this->requestStack = $requestStack;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('mase_fuc_lcl', [$this, 'createLocationChangeLink'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('mase_fuc_get_ma', [$this, 'getMaseParamFromYml']),
            new \Twig_SimpleFunction('mase_fuc_get_const', [$this, 'getConstValueFromYml']),
            new \Twig_SimpleFunction('mase_fuc_ca', [$this, 'createAtag'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('mase_fuc_cpn', [$this, 'createPaginationFromPaginator'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('mase_fuc_cpnfs', [$this, 'createPagination'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('mase_fuc_get_ht', [$this, 'getHeaderInfoForTitle']),
            new \Twig_SimpleFunction('mase_fuc_get_hd', [$this, 'getHeaderInfoForDescription']),
            new \Twig_SimpleFunction('mase_fuc_get_hk', [$this, 'getHeaderInfoForKeywords']),
            new \Twig_SimpleFunction('mase_fuc_get_sn', [$this, 'getSiteName']),
            new \Twig_SimpleFunction('mase_fuc_get_css', [$this, 'getHeaderInfoForCss'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('mase_fuc_put_css_test', [$this, 'putCssTest']),
            new \Twig_SimpleFunction('mase_fuc_get_case_types', [$this, 'getCaseTypes']),
            new \Twig_SimpleFunction('mase_fuc_get_product_types', [$this, 'getProductTypes']),
            new \Twig_SimpleFunction('mase_fuc_get_reserve_target_manage_types', [$this, 'getReserveTargetManageTypes']),
            new \Twig_SimpleFunction('mase_fuc_get_reserve_target_types', [$this, 'getReserveTargetTypes']),
            new \Twig_SimpleFunction('mase_fuc_free_space', [$this, 'getFreeSpace'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('mase_fuc_has_container', [$this, 'hasMediaDataContainer']),
            new \Twig_SimpleFunction('mase_fuc_bcadd', [$this, 'reserveBcAdd']),
            new \Twig_SimpleFunction('mase_fuc_bcmul', [$this, 'reserveBcMul']),
            new \Twig_SimpleFunction('mase_fuc_get_file', [$this, 'getFileEntity']),
            new \Twig_SimpleFunction('mase_fuc_get_pa', [$this, 'getProductApprovedAt']),
            new \Twig_SimpleFunction('mase_fuc_bitflag', [$this, 'bitFlagToData']),
            new \Twig_SimpleFunction('mase_fuc_is_mobile', [$this, 'isMobile']),
            new \Twig_SimpleFunction('mase_fuc_is_apl', [$this, 'isApl']),
            new \Twig_SimpleFunction('mase_fuc_get_ml_open_tracking_url', [$this, 'createMailOpenTrackingUrl']),
            new \Twig_SimpleFunction('mase_fuc_replace_ml_click_tracking_url', [$this, 'replaceOriginalUrlsToClickTrackingUrlsInMailBody']),
            new \Twig_SimpleFunction('mase_fuc_can_show_item', [$this, 'canShowItem']),
            new \Twig_SimpleFunction('mase_fuc_asset_by_lang', [$this, 'getAssetUrlByLang']),
        ];
    }
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('mase_fil_ken', [$this, 'convertIdToTdfkName']),
            new \Twig_SimpleFilter('mase_fil_city', [$this, 'convertIdToCityName']),
            new \Twig_SimpleFilter('mase_fil_code', [$this, 'convertIdToWording']),
            new \Twig_SimpleFilter('mase_fil_codes', [$this, 'convertIdsToWording']),
            new \Twig_SimpleFilter('mase_fil_class', [$this, 'convertRequestToClass']),
            new \Twig_SimpleFilter('mase_fil_enum', [$this, 'convertEnumToWording']),
            new \Twig_SimpleFilter('mase_fil_codecnf_codes', [$this, 'convertCodeIdsToWording']),
            new \Twig_SimpleFilter('mase_fil_codecnf_enum', [$this, 'convertCodeEnumToWording']),
            new \Twig_SimpleFilter('mase_fil_currency', [$this, 'showCurrency']),
            new \Twig_SimpleFilter('mase_fil_numeric', [$this, 'showNumeric']),
            new \Twig_SimpleFilter('mase_fil_youtube', [$this, 'replaceYoutube'], ['pre_escape'=>'html', 'is_safe' => ['html']]),
            new \Twig_SimpleFilter('mase_fil_mask', [$this, 'mask']),
            new \Twig_SimpleFilter('mase_fil_open_range', [$this, 'openRangeMessage']),
            new \Twig_SimpleFilter('mase_fil_tag', [$this, 'createTagStr']),
            new \Twig_SimpleFilter('mase_fil_truncate', [$this, 'truncateString']),
            new \Twig_SimpleFilter('mase_fil_datetime', [$this, 'formatDateTime']),
            new \Twig_SimpleFilter('mase_fil_open_range_form', [$this, 'getOpenRangeForm']),
        ];
    }
    public function createPagination($totalCount, $onePageCount, $request)
    {
        return $this->_createPagination($totalCount, $onePageCount, $request);
    }

    public function createPaginationFromPaginator(Paginator $paginator, $request)
    {
        return $this->_createPagination($paginator->count(), $paginator->getQuery()->getMaxResults(), $request);
    }

    private function _createPagination($totalCount, $onePageCount, $currentRequest, $sliding = 3)
    {
        /**
         * 開発メモ
         * 当初、引数で指定された$currentRequestを用いて処理を行っていたが、
         * コントローラにてforwardを行った場合、オリジナルのリクエストより、
         * 各種値を取得しないと、forwardを行った際のパラメータからurlを作成してしまうので、
         * マスターリクエストを取得するようにした。
         */
        $request = $this->requestStack->getMasterRequest();

        if (null === $routeName = $request->attributes->get('_route')) {
            return '';
        }

        if (null === $routeParams = $request->attributes->get('_route_params')) {
            return '';
        }
       
        $getParams = $request->query;

        $nowPage = $getParams->getInt('page', 1);
        $maxPage = (int) ceil($totalCount / $onePageCount);

        if (0 === $totalCount) {
            return '';
        }

        $newRouteParams = array_merge($getParams->all(), $routeParams);
      
        $firstLink = '';
      
        if (1 !== $nowPage) {
            $newRouteParams['page'] = 1;
            $firstLink = '<a href="'.$this->router->generate($routeName, $newRouteParams, UrlGeneratorInterface::ABSOLUTE_PATH).'">最初へ</a> ';
        }
        $firstLink = '<a href="'.$this->router->generate($routeName, $newRouteParams, UrlGeneratorInterface::ABSOLUTE_PATH).'">最初へ</a> ';
        $backLink = '';
        if (1 !== $nowPage) {
            $newRouteParams['page'] = $nowPage - 1;
            $backLink = '<a href="'.$this->router->generate($routeName, $newRouteParams, UrlGeneratorInterface::ABSOLUTE_PATH).'"> 前へ</a> ';
        }
        $maxSliding = $sliding * 2;
        $backPageLink = '';
        $backPageLinks = [];
        if (1 !== $nowPage) {
            for ($i = 0; $maxSliding > $i; $i++) {
                $page = $nowPage - ($i + 1);
                if (0 >= $page) {
                    break;
                }
                $newRouteParams['page'] = $page;

                array_push($backPageLinks, ('<a href="'.$this->router->generate($routeName, $newRouteParams, UrlGeneratorInterface::ABSOLUTE_PATH).'">'.$page.'</a> '));
            }
        }

        $nextPageLink = '';
        $nextPageLinks = [];
        if ($maxPage !== $nowPage) {
            for ($i = 0; $maxSliding > $i; $i++) {
                $page = $nowPage + ($i + 1);
                if ($maxPage < $page) {
                    break;
                }
                $newRouteParams['page'] = $page;
                array_push($nextPageLinks, ('<a href="'.$this->router->generate($routeName, $newRouteParams, UrlGeneratorInterface::ABSOLUTE_PATH).'">'.$page.'</a> '));
            }
        }
        if ($sliding <= count($backPageLinks)) {
            if ($sliding <= count($nextPageLinks)) {
                for ($i = 0; $sliding > $i; $i++) {
                    $backPageLink = $backPageLinks[$i] . $backPageLink;
                }
                for ($i = 0; $sliding > $i; $i++) {
                    $nextPageLink .= $nextPageLinks[$i];
                }
            } else {
                for ($i = 0; ($sliding + ($sliding - count($nextPageLinks))) > $i; $i++) {
                    /**
                     * 開発メモ
                     * 「($sliding + ($sliding - count($nextPageLinks)))」の計算結果が$slidingと同じ値になると
                     * こけるので、以下の判定をいれました。
                     */
                    if (count($backPageLinks) <= $i) {
                        continue;
                    }
                    $backPageLink = $backPageLinks[$i] . $backPageLink;
                }
                for ($i = 0; count($nextPageLinks) > $i; $i++) {
                    $nextPageLink .= $nextPageLinks[$i];
                }
            }
        } else {
            if ($sliding <= count($nextPageLinks)) {
                for ($i = 0; count($backPageLinks) > $i; $i++) {
                    $backPageLink = $backPageLinks[$i] . $backPageLink;
                }
                for ($i = 0; ($sliding + ($sliding - count($backPageLinks))) > $i; $i++) {
                    /**
                     * 開発メモ
                     * 「($sliding + ($sliding - count($backPageLinks)))」の計算結果が$slidingと同じ値になると
                     * こけるので、以下の判定をいれました。
                     */
                    if (count($nextPageLinks) <= $i) {
                        continue;
                    }
                    $nextPageLink .= $nextPageLinks[$i];
                }
            } else {
                for ($i = 0; count($backPageLinks) > $i; $i++) {
                    $backPageLink = $backPageLinks[$i] . $backPageLink;
                }
                for ($i = 0; count($nextPageLinks) > $i; $i++) {
                    $nextPageLink .= $nextPageLinks[$i];
                }
            }
        }

        $nextLink = '';
        if ($maxPage !== $nowPage) {
            $newRouteParams['page'] = $nowPage + 1;
            $nextLink = '<a href="'.$this->router->generate($routeName, $newRouteParams, UrlGeneratorInterface::ABSOLUTE_PATH).'">次へ</a> ';
        }

        $lastLink = '';
        if ($maxPage !== $nowPage) {
            $newRouteParams['page'] = $maxPage;
            $lastLink = '<a href="'.$this->router->generate($routeName, $newRouteParams, UrlGeneratorInterface::ABSOLUTE_PATH).'">最後へ</a> ';
        }

        return $firstLink.$backLink.$backPageLink.' <b>'.$nowPage.'</b> '.$nextPageLink.$nextLink.$lastLink;
    }
}