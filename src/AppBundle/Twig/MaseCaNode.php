<?php

namespace AppBundle\Twig;

class MaseCaNode extends \Twig_Node
{
    public function __construct(\Twig_Node_Expression $routeName, \Twig_Node_Expression $subject, \Twig_Node_Expression $routeParams, \Twig_Node_Expression $attribute, \Twig_Node_Expression $relative, \Twig_Node $body, $lineno = 0, $tag = null)
    {
        $nodes = [
            'routeName' => $routeName, 
            'subject' => $subject, 
            'routeParams' => $routeParams, 
            'attribute' => $attribute, 
            'relative' => $relative, 
            'body' => $body,
        ];
        parent::__construct($nodes, array(), $lineno, $tag);
    }

    public function compile(\Twig_Compiler $compiler)
    {
        $compiler
            ->addDebugInfo($this)
            ->write('$aTag = $this->env->getExtension(\'AppBundle\Twig\MaseExtension\')->createAtagStart('."\n")
            ->subcompile($this->getNode('routeName'))
            ->raw(",\n")
            ->subcompile($this->getNode('subject'))
            ->raw(",\n")
            ->subcompile($this->getNode('routeParams'))
            ->raw(",\n")
            ->subcompile($this->getNode('attribute'))
            ->raw(",\n")
            ->subcompile($this->getNode('relative'))
            ->raw("\n")
            ->write(');'."\n")
            ->write('if (\'\' !== $aTag) {'."\n")
            ->write('echo $aTag;'."\n")
            ->subcompile($this->getNode('body'))
            ->write('echo $this->env->getExtension(\'AppBundle\Twig\MaseExtension\')->createAtagEnd();'."\n")
            ->write('}'."\n")
        ;
    }
}
