<?php

namespace AppBundle\Twig;

class MaseCpaTokenParser extends \Twig_TokenParser
{
    public function parse(\Twig_Token $token)
    {
        $lineno = $token->getLine();
        $stream = $this->parser->getStream();

        $targetUser = new \Twig_Node_Expression_Constant('', $lineno);
        $attribute = new \Twig_Node_Expression_Array(array(), $lineno);

        if (!$stream->test(\Twig_Token::BLOCK_END_TYPE)) {
            if ($stream->test('with')) {
                $stream->next();
                $targetUser = $this->parser->getExpressionParser()->parseExpression();
            }
            if ($stream->test('attribute')) {
                $stream->next();
                $attribute = $this->parser->getExpressionParser()->parseExpression();
            }
        }
        $stream->expect(\Twig_Token::BLOCK_END_TYPE);

        $body = $this->parser->subparse(array($this, 'decideMaseTagCpaEnd'), true);
        $stream->expect(\Twig_Token::BLOCK_END_TYPE);

        return new MaseCpaNode($targetUser, $attribute, $body, $lineno, $this->getTag());
    }

    public function decideMaseTagCpaEnd(\Twig_Token $token)
    {
        return $token->test('endmase_tag_cpa');
    }

    public function getTag()
    {
        return 'mase_tag_cpa';
    }
}
