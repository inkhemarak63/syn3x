<?php

namespace AppBundle\Twig;

class MaseCaTokenParser extends \Twig_TokenParser
{
    public function parse(\Twig_Token $token)
    {
        $lineno = $token->getLine();
        $stream = $this->parser->getStream();

        // {% mase_tag_ca 'bar' %}
        $routeName = new \Twig_Node_Expression_Constant('', $lineno);
        $subject = new \Twig_Node_Expression_Constant(null, $lineno);
        $routeParams = new \Twig_Node_Expression_Array(array(), $lineno);
        $attribute = new \Twig_Node_Expression_Array(array(), $lineno);
        $relative = new \Twig_Node_Expression_Constant(false, $lineno);

        if (!$stream->test(\Twig_Token::BLOCK_END_TYPE)) {
            if ($stream->test('with')) {
                $stream->next();
                $routeName = $this->parser->getExpressionParser()->parseExpression();
            }
            if ($stream->test('subject')) {
                $stream->next();
                $subject = $this->parser->getExpressionParser()->parseExpression();
            }
            if ($stream->test('param')) {
                $stream->next();
                $routeParams = $this->parser->getExpressionParser()->parseExpression();
            }
            if ($stream->test('attribute')) {
                $stream->next();
                $attribute = $this->parser->getExpressionParser()->parseExpression();
            }
            if ($stream->test('relative')) {
                $stream->next();
                $relative = $this->parser->getExpressionParser()->parseExpression();
            }
        }
        $stream->expect(\Twig_Token::BLOCK_END_TYPE);

        // {% endmase_tag_ca %}
        $body = $this->parser->subparse(array($this, 'decideMaseTagCaEnd'), true);
        $stream->expect(\Twig_Token::BLOCK_END_TYPE);

        return new MaseCaNode($routeName, $subject, $routeParams, $attribute, $relative, $body, $lineno, $this->getTag());
    }

    public function decideMaseTagCaEnd(\Twig_Token $token)
    {
        return $token->test('endmase_tag_ca');
    }

    public function getTag()
    {
        return 'mase_tag_ca';
    }
}
