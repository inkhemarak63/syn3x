<?php
namespace AppBundle\Form\MaseUserSearch;

use AppBundle\Form\MaseSuperType;
use AppBundle\MaseCodeConfig;
use AppBundle\Repository\Condition\TodoSearch\TodoSearchNormalCondition;
use AppBundle\Util\MaseUtil;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class MaseUserSearchNormalType extends MaseSuperType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $rangeIntValidator = $this->createRangeIntValidator();
        $regexIntValidator = $this->createRegexIntValidator();

        $builder
            ->add('minItemNumInt01', TextType::class,
                [
                    'label' => 'TOEICスコアの最小値',
                    'required' => false,
                    'constraints' => [$rangeIntValidator, $regexIntValidator]
                ]
            )
            ->add('maxItemNumInt01', TextType::class,
                [
                    'label' => 'TOEICスコアの最大値',
                    'required' => false,
                    'constraints' => [
                        $rangeIntValidator,
                        $regexIntValidator,
                        new GreaterThanOrEqual([
                            'propertyPath' => 'parent.viewData[minItemNumInt01]',
                            'message' => '最大値は最小値よりも大きい値を入力してください'
                        ])
                    ]
                ]
            )
            ->add('search', SubmitType::class, ['label' => '検索']);

        $this->addCheckBoxType($builder, 'itemCodeVarchar06', false, '希望業種', true, MaseCodeConfig::USERINFO_INDUSTRY );
        $this->addCheckBoxType($builder, 'itemCodeVarchar09', false, '希望職種', true, MaseCodeConfig::USERINFO_OCCUPATION );

        $kenChoices = $this->getKenChoices($options['registry'], $options['lang']);
        $this->addSelectBoxSingleType($builder, 'itemCodeVarchar12', false, '希望勤務地', true, $kenChoices);

        $codeId = 'S001';
        $this->addCodeFiled(
            $options['registry'],
            $builder,
            $options['lang'],
            $codeId,
            [
                [
                    'fieldName' => 'itemCodeVarchar01',
                    'required'  => false,
                    'label'     => '学部',
                ],
                [
                    'fieldName' => 'itemCodeVarchar02',
                    'required'  => false,
                    'label'     => '学科・専攻科',
                ]
            ]
        );

        $this->addSelectBoxSingleType($builder, 'sort', false, '並び順', true, [
            '新着順' => 'createdAt'
        ]);

        $builder->addModelTransformer(new CallbackTransformer(
            fn($initValue) => [],
            fn(array $input) => $this->createSearchCondition($input, $options)
        ));

        return $builder;
    }

    /**
     * @param array $input
     * @param array $options
     * @return MaseUserSearchNormalCondition
     */
    private function createSearchCondition(array $input, array $options): MaseUserSearchNormalCondition
    {
        return (new MaseUserSearchNormalCondition)
            ->setSort(MaseUserSearchNormalCondition::SORT_CREATED_AT)
            ->setMinItemNumInt01(MaseUtil::toInteger($input['minItemNumInt01']))
            ->setMaxItemNumInt01(MaseUtil::toInteger($input['maxItemNumInt01']))
            ->setItemCodeVarchar01($input['itemCodeVarchar01'])
            ->setItemCodeVarchar02($input['itemCodeVarchar02'])
            ->setItemCodeVarchar06($input['itemCodeVarchar06'])
            ->setItemCodeVarchar09($input['itemCodeVarchar09'])
            ->setItemCodeVarchar12($input['itemCodeVarchar12']);
    }
}
