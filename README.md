# Account
user: admin
pwd: kbb12345

# New project 
composer create-project symfony/framework-standard-edition my_project_name "3.4.*"

# Run Server cd my-project/
run cli: 
symfony server:start
symfony server:stop
run dev: symfony server:start -d 
run bin: php bin/console server:start
sudo rm -rf var/cache/*
git stash
git clean -df #discard all file change


kill -9 26798
lsof -wni tcp:8000

git reset --hard #discus all
# Lesson 
https://symfony.com/doc/current/index.html
sudo kill -9 $(sudo lsof -t -i:8000)

php bin/console doctrine:database:create
php bin/console cache:clear 

# Migrage Db
php bin/console doctrine:migrations:generate

# create DB
php bin/console doctrine:database:create
# Create new table
php bin/console doctrine:generate:entity
# Migrate to db
php bin/console doctrine:schema:update --force

composer require doctrine/orm "^2.5"
composer require javihgil/doctrine-pagination:2.0

# Creating an Entity Class
php bin/console doctrine:generate:entity
Enter Name Eg: AppBundle:Product
Configuration format (yml, xml, php, or annotation) [annotation]: annotation

# OR 
php bin/console doctrine:generate:entities AppBundle/Entity/Product

git stash
git clean -df #discard all file change

git reset --hard #discus all